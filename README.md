Web of Stoppages
----------------
[https://alternatifbilisim.org/wosp/](https://alternatifbilisim.org/wosp/)

WoSP, erişime engellenen URL'leri listeleyen bir Alternatif Bilişim projesidir.

WoSP veri tabanında bulunan URL'lerin büyük bir bölümü, engelliweb.com'un 2015 ortalarında kopyalanan/arşivlenen içeriğinden oluşmaktadır. 2009 ve öncesine ait girdiler, bu tarihlerde erişim engellerini listeleyen farklı sayfaların arşiv kayıtlarından; son 1-2 yıla ait olanlar ise muhtelif web yayınlarında bulunan ekran görüntüleri, referans bilgiler, haberler, vb. kayıtlardan toparlanmıştır.

Listenin son sıralarında bulunan "451" tarihli kayıtlar, 2007 öncesine ait. Fakat yıllarına dair net bilgi olmadığından bu şekilde girildi.

Public Domain - Herhangi bir telif hakkı kısıtlaması yoktur.