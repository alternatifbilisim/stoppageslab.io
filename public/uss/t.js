      $(document).ready( function () {
       $('#data-table').dataTable( {
        "ordering": false,
          "ajax": "v/twitter.json",
          "deferRender": true,
          "columns": [
         { "data": "user" }
         ],
          "language": {
           "sInfo":           "Erişime engellenen Twitter hesabı sayısı: _TOTAL_",
           "infoEmpty":       "Erişime engellenen Twitter hesabı sayısı: ...",
	   "sInfoFiltered":   "(arama filtresi ile eşleşen)",
           "sLengthMenu":     "Bir sayfada gösterilecek girdi sayısı: _MENU_",
	   "sLoadingRecords": "Yükleniyor...",
           "sProcessing":     "İşleniyor...",
           "sSearch":         "Ara:",
	   "sZeroRecords":    "Eşleşen kayıt bulunamadı",
	   "oPaginate": {
		"sNext":     ">",
		"sPrevious": "<"
	   },
          },

          "dom": '<"top"iLf>t<"bottom"p><"clear">'

           });
    } );

