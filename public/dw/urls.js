      $(document).ready( function () {
       $('#data-table').dataTable( {
          "ajax": "i.json",
          "ordering": false,
          "deferRender": true,
          "columns": [
         { "data": "tarih" },
         { "data": "url" },
         { "data": "tanim"}
         ],
          "language": {
           "sInfo":           "Erişime engellenen URL sayısı: _TOTAL_",
           "infoEmpty":       "Erişime engellenen URL sayısı: ...",
           "sInfoFiltered":   "(arama filtresi ile eşleşen)",
           "sLengthMenu":     "Bir sayfada gösterilecek girdi sayısı: _MENU_",
           "sLoadingRecords": "Yükleniyor...",
           "sProcessing":     "İşleniyor...",
           "sSearch":         "Ara:",
           "sZeroRecords":    "Eşleşen kayıt bulunamadı.",
           "oPaginate": {
                "sNext":     ">",
                "sPrevious": "<"
           },
          },
          "dom": '<"top"iLf>t<"bottom"p><"clear">'
         });
       });

